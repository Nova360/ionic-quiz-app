import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Slides } from 'ionic-angular';

/**
 * Generated class for the SliderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html',
})
export class SliderPage {

@ViewChild(Slides) slides :Slides;
count  : number = 0;
totalpts :string;
  constructor(private toast: ToastController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SliderPage');
  }

  Tally (value :string) 
  {
    if(value == "correct")
    {
      this.count++;
      //console.log(this.count);
    } 
    this.slides.slideNext( 1000);
    this.slides.lockSwipeToPrev(true);
  }

  FinalScore(value :string){
    if(value == "correct")
    {
      this.count++;
      this.totalpts = this.count.toString();
      //console.log(this.count);
    } 
   // this.totalpts = this.count.toString();
    this.slides.lockSwipeToPrev(true);
    this.toast.create({message: "Your Score: " + this.totalpts}).present();
  }

}
